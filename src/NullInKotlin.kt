
//Computational classes/ functions already exists either in standard lib by language or popular third party libs
//Functions should be as pure as possible, i.e. the function has to perform only one task. They need to be simple


// Question Mark ? after type permits Null
// An attempt to reinvent the wheel, but, its worthless
fun safeStringToInt(input: String):Int?{
    return when(input){
        "0" -> 0
        "1" -> 1
        "2" -> 2
        "3" -> 3
        "4" -> 4
        else -> null
    }
}


fun main(args: Array<String>){
    if(args[0].isNotEmpty()){ //args[0].length>0 also does the same but isNotEmpty() is the efficient way as it need not count exact size
        println(safeStringToInt(args[0]))
    }
    // Kotlin standard function to perform as above & more (unlimited integers)
    println(args[0].toIntOrNull())
}
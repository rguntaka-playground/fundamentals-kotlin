/*
 * class
 */

class School(
    // contains constructor and properties
    val name: String,
    val studentsCount: Double
){
    // class body, and this contains functions and other ref variables
    fun isStudentCountHigh():Boolean{
        return true
    }
}

// data keyword auto generate getters & setters for this class, equals(), hashcode(), toString(), etc and
// these functions can be overriden
data class Student(
    val name: String,
    val totalMarks: Double = 0.0,
    val profilePic: String = "random"
){

}

val student1 = Student("Jhonson", 488.0) // represents first two variables and profilePic will hold default value
val student2 = Student("Ram", 568.0, "selfie.jpg")
val student3 = Student("Thomson", profilePic = "selfie.jpg") // explicit calling of an variable name


//Cohesion: The degree to which the things belong together.
//Its subjective concept

class DogOne(
    val age: Int,
    val weight: Double,
    val height: Double,
    val name: String,
    val owner: Student){
    fun makeNoise(){
        println("Bark!!")
    }
}

// Not cohesive. example: having UI widgets inside DB. Or, having Model classes inside Views

class DogTwo(
    val bankAccountNumber: Int,
    val fluentInGerman: Boolean)
{
    fun measureGravity(){
        println("Bark!!")
    }
}

// Result wrapper (Either Monad) is a quality from the functional programming
// multiple child classes in the class
sealed class Result{
    // object works if there is no need of input data (getters/ setters)
    object Loading: Result()
    object Error: Result()
    data class Success(val value: String): Result()
}

// mimic interaction between different components of an program
fun requestData(){
    val result = InputOutDevice.getData()
    when(result){
        is Result.Loading -> UserInterface.showLoadingScreen()
        is Result.Success -> UserInterface.showData(result.value)
        is Result.Error -> UserInterface.showErrorMessage()
    }
}

object InputOutDevice{
    fun getData(): Result{
        return Result.Success("Sample Input Value")
    }
}

object UserInterface{
    fun showLoadingScreen(){}
    fun showData(data:String){}
    fun showErrorMessage(){}
}

// Restricted set of data: Enum class
enum class MathsOperator(val operator: Char){
    PLUS('+'),
    MINUS('-'),
    MULTIPLY('*'),
    DIVIDE('/')
}

// this function returns another function based on the situation
// Function Type syntax (Double,Double) ->Double, (input, input) -> (return)
// Return type is different
fun checkOperator(operator: MathsOperator): (Double,Double) -> Double{
    return when(operator){
        // lambda expression: functions without any name. it may contain input arguments, return data
        MathsOperator.PLUS -> { x,y -> x+y}
        MathsOperator.MINUS -> { x,y -> x-y}
        MathsOperator.MULTIPLY -> { x,y -> x*y}
        MathsOperator.DIVIDE -> { x,y -> x/y}
    }
}

fun main(args:Array<String>){
    val mathFunction = checkOperator(MathsOperator.MINUS) // here, function will be returned. Example: {} from checkOperator()
    println(mathFunction(4.0,2.0))
}
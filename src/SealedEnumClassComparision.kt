
// Result wrapper (Either Monad) is a quality from the functional programming
// multiple child classes in the class

// sealed class can't be instantiated
// constructor of sealed class always private
// In general, sealed classes mostly used with When expression


sealed class Father{
    class child1: Father()
    class child2: Father()
    object childObj: Father()
}

fun family(member: Father){
    // Note: is command only required for classes, observe the difference in syntax for objects
    when(member){
        is Father.child1 -> println("child1")
        is Father.child2 -> println("child2")
        Father.childObj -> println("child object")
    }
}


// sealed vs enum
// sealed class allows to create classes of different types whereas in Enum all elements should be similar

enum class Days(day: String){
    Sunday("one"),
    Monday("two") //Monday(2) is not allowed
}

sealed class SealedDays{
    class Sunday(day: String): SealedDays() //Sunday accepts string whereas Monday accepts int
    class Monday(day: Int): SealedDays()
    object Tuesday: SealedDays() // observe the syntax difference with respect to Object
}

fun pickDay(day: SealedDays){
    when(day){
        is SealedDays.Sunday -> println("Sunday")
        is SealedDays.Monday -> println("Monday")
        SealedDays.Tuesday -> println("Tuesday")
    }
}


fun main(args: Array<String>){
    //instantiation is happening here for class child1 with child1(), observe syntax diff for childobj
    var c1 = Father.child1()
    family(c1)

    var cobj = Father.childObj
    family(cobj)


    // SealedDays

    val sunday = SealedDays.Sunday("one")
    val monday = SealedDays.Monday(2)
    val tuesday = SealedDays.Tuesday

    pickDay(sunday)
    pickDay(monday)
    pickDay(tuesday)
}

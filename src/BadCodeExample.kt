
// the whole logic in the single function

fun main(args: Array<String>){

    val mathOperator = args[0]
    var operand1 = 0.0
    var operand2 = 0.0
    var validArguments = false

    when(mathOperator){
        "+" -> validArguments = true
        "-" -> validArguments = true
        "*" -> validArguments = true
        "/" -> validArguments = true
    }

    if(args[1].toDoubleOrNull()==null) validArguments = false
    else operand1 = args[1].toDouble()

    if(args[2].toDoubleOrNull()==null) validArguments = false
    else operand2 = args[2].toDouble()

    if(validArguments){
        val result: String

        when(mathOperator){
            "+" -> result = (operand1 + operand2).toString()
            "-" -> result = (operand1 - operand2).toString()
            "*" -> result = (operand1 * operand2).toString()
            "/" -> result = (operand1 / operand2).toString()
            else -> result = "Error occured"
        }

        println(result)
    } else {
        println("Error occured")
    }

}
// val is similar to final keyboard of Java. Its run time constant
// visibility modifier private
// Here, the variable can be mutable, ex: List can add or remove items.

private val ADD: String = "+"
private val SUBSTRACT: String = "-"
private val MULTIPLY: String = "*"
private val DIVIDE: String = "/"


// -> compile time const. Compiler can allocate specific memory in much efficient way, which is kind of fixed and
// this increases the efficiency.
// -> const val can't be inside the function as Kotlin invokes function at run time where as const val gets
// memory allocated at compile time.
// -> Also, this variable is immutable
// Naming convention has to be Upper Snake Case, i.e. Caps with Underscore

const val ERROR_MESSAGE = "Error Occured"

// var keyword represents a variable

var currentLocationX = 0
var currentLocationY = 0



/*
 * Main, Fun, When
 */



fun calculate(operator:String, operand1: Double, operand2:Double): String{
    return when(operator){
        ADD -> (operand1 + operand2).toString()
        SUBSTRACT -> (operand1 - operand2).toString()
        MULTIPLY -> (operand1 * operand2).toString()
        DIVIDE -> (operand1 / operand2).toString()
        else -> ERROR_MESSAGE
    }
}

// Add configuration + 4 6 and the result will be 10. Play around with various configurations

fun main(args: Array<String>){
    val operatorSymbol = args[0]
    val operand1 = args[1]
    val operand2 = args[2]

    println(calculate(operatorSymbol,operand1.toDouble(),operand2.toDouble()))
}
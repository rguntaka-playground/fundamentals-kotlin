//input is array
//solve it if the items form a valid expression
// 1. Array must have length of 3
// 2. First & Second singles must be valid doubles
// 3. Third string must be valid operator

fun main(args:Array<String>){
    solveExpresson(args)
}

const val ERROR_NOT_VALID_ARGUMENTS = "Error: program needs 2 numbers and an operator (+,-,*,/) as arguments"
const val ERROR_NOT_VALID_NUMBERS = "Error: first 2 arguments should be numbers"
const val ERROR_NOT_VALID_OPERATOR = "Error: valid operators are (+,-,*,/) "


fun solveExpresson(args:Array<String>): String {
    if (args.size != 3)
        return ERROR_NOT_VALID_ARGUMENTS


    if (firstAndSecondNotValidNumbers(args[0], args[1])) {
        return ERROR_NOT_VALID_NUMBERS
    }


    return solve(args[0].toDouble(), args[1].toDouble(), getOperation(args[2]))

}


fun getOperation(operator:String): (Double, Double) -> String{
    return when(operator){
        "+" -> { x,y -> (x+y).toString()}
        "-" -> { x,y -> (x-y).toString()}
        "*" -> { x,y -> (x*y).toString()}
        "/" -> { x,y -> (x/y).toString()}
        else -> {x,y -> getErrorMessage()}
    }
}

// Higher order function, a function that either accepts as argument or returns function
fun solve(operand1: Double, operand2: Double, operation: (Double, Double) -> String):String = operation.invoke(operand1,operand2)

fun getErrorMessage() = ERROR_NOT_VALID_OPERATOR

fun firstAndSecondNotValidNumbers(s1:String,s2:String):Boolean {
  if (s1.toDoubleOrNull() == null || s2.toDoubleOrNull() == null) //check if s1 and s2 are valid Doubles and return
        return true
  return false
}
